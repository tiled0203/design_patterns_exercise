
package com.dsl.design.pattern.proxy;

//TODO: implement IConnection interface
//the connect method has to check that realConnection field if not null, if so then instantiate it. Otherwise call the connect method
public class ProxyConnection {
    private final String url;
    private RealConnection realConnection;

    public ProxyConnection(String url) {
        this.url = url;
    }


}
