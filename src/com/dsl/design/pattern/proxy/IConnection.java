
package com.dsl.design.pattern.proxy;

public interface IConnection
{
    void connect();
}
