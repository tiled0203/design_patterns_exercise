
package com.dsl.design.pattern.facade;

public interface IBank
{
    void withdraw(double amount);
}
