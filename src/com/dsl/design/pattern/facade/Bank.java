
package com.dsl.design.pattern.facade;

//Facade
/*TODO: implement IBank and use AccountValidator and WithdrawalValidator subsystems to check if you have sufficient money
and to check if the bank account exists
WithdrawalValidator returns 0 if Account savings insufficient to withdrawal
WithdrawalValidator returns 1 if Account savings must at least remains 20 dollars after withdrawal
WithdrawalValidator returns 2 if Withdraw is successful!
* */
public class Bank {
    private final Account account;

    public Bank(Account account) {
        this.account = account;
    }


}
