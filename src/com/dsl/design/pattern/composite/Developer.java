
package com.dsl.design.pattern.composite;

//Leaf
//TODO: implement Employee interface
public class Developer {
    private final String name;
    private final double salary;
    private final String position;

    public Developer(String name, double salary, String position) {
        this.name = name;
        this.salary = salary;
        this.position = position;
    }

 /*
Print out example of the printDetails method
Name: Chris Salary: 3000,00 Position: Junior Developer
*/
}
