
package com.dsl.design.pattern.composite;

public interface Employee
{
    void printDetails();
}
