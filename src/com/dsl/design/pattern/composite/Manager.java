
package com.dsl.design.pattern.composite;

import java.util.ArrayList;
import java.util.List;

//Composite
//TODO: Implement Employee interface
public class Manager {
    private final String name;
    private final double salary;
    private final String position;
    private final List<Employee> employees;

    public Manager(String name, double salary, String position) {
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public String getPosition() {
        return position;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

/*Print out example of the printDetail method
=============== Management ===============
Name: Isabelle Salary: 6000,00 Position: Marketing Manager
=============== Management ===============
Name: Paul salary: 5000,00 position: Executive Salesman
Name: Mark salary: 3000,00 position: Salesman
*/
}
