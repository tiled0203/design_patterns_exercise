
package com.dsl.design.pattern.composite;

//Leaf
//TODO: implement Employee interface
public class Salesman {
    private final String name;
    private final double salary;
    private final String position;

    public Salesman(String name, double salary, String position) {
        this.name = name;
        this.salary = salary;
        this.position = position;
    }

    /*
Print out example of the printDetails method
Name: Paul salary: 5000,00 position: Executive Salesman
*/

}
