
package com.dsl.design.pattern.adapter;

public interface Converter
{
    void convert();
}
