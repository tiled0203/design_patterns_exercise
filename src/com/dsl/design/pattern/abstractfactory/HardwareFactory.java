
package com.dsl.design.pattern.abstractfactory;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class HardwareFactory {
    private HardwareFactory() {
    }

    /*TODO: create a switch case statement that calls the create methods from the different hardware factories (MouseFactory, KeyboardFactory)
        Based on boolean isGaming it will send a String (gaming or basic) as an argument to the create method */
    public static IHardware create(String type, boolean isGaming) {
        throw new NotImplementedException();
    }
}
