
package com.dsl.design.pattern.abstractfactory;

public interface IHardware
{
    void create();
}
