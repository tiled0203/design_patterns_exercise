
package com.dsl.design.pattern.state.example1;

public interface State
{
    void printCurrentState();
}
