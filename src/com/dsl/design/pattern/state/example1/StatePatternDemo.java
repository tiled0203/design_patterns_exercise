
package com.dsl.design.pattern.state.example1;

public class StatePatternDemo
{
    //TODO: different states just needs to implement the state pattern, it only has to print the current state
    public static void main(String[] args) {
        StartState startState = new StartState();
        WaitingState waitingState = new WaitingState();
        RestartState restartState = new RestartState();
        GameOverState gameOverState = new GameOverState();

//        Game game = new Game(startState);
//        game.printCurrentState();
//        System.out.println("Playing...");
//        System.out.println("Character died");
//        game.setState(waitingState);
//        game.printCurrentState();
//        System.out.println("Do you want to restart? Yes");
//        game.setState(restartState);
//        game.printCurrentState();
//        System.out.println("Playing...");
//        System.out.println("Character died");
//        game.setState(waitingState);
//        game.printCurrentState();
//        System.out.println("Do you want to restart? No");
//        game.setState(gameOverState);
//        game.printCurrentState();
    }
}
