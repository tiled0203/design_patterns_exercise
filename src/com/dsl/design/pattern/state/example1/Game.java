
package com.dsl.design.pattern.state.example1;

public class Game implements State {
    private State state;

    public Game(State state) {
        this.state = state;
    }

    @Override
    public void printCurrentState()
    {
        state.printCurrentState();
    }

    public void setState(State state)
    {
        this.state = state;
    }
}
