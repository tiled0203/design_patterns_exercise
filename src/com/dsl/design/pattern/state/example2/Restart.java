
package com.dsl.design.pattern.state.example2;

public class Restart extends GameState
{
    @Override
    public void printState()
    {
        System.out.println("CURRENT STATE: Restart game");

    }
}
