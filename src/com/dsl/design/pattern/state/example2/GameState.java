
package com.dsl.design.pattern.state.example2;

public abstract class GameState
{
    public abstract void printState();
}
