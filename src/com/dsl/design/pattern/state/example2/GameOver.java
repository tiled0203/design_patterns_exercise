
package com.dsl.design.pattern.state.example2;

public class GameOver extends GameState
{
    @Override
    public void printState()
    {
        System.out.println("CURRENT STATE: Game Over!");
    }
}
