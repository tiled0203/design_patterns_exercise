
package com.dsl.design.pattern.state.example2;

//FOR EXAMPLE PURPOSES, IT'S ALREADY IMPLEMENTED
public class StatePatternDemo
{
    public static void main(String[] args) {
        new MyGame().test();
    }
}
