
package com.dsl.design.pattern.command;

public interface IController
{
    void execute();
    void unexecute();
}
